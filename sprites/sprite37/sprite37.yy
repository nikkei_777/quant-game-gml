{
    "id": "89de584b-c3f3-4bfc-88c5-91513f2b698d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite37",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33646175-b2ef-47f1-a15b-4599e72650c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89de584b-c3f3-4bfc-88c5-91513f2b698d",
            "compositeImage": {
                "id": "d4c3d95c-5e4c-42d2-a44e-c1fdfd8c355d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33646175-b2ef-47f1-a15b-4599e72650c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d431b43-619b-48fe-9441-27fbbcac8320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33646175-b2ef-47f1-a15b-4599e72650c4",
                    "LayerId": "259f370c-02f5-4a6c-beab-0cc3128c51ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "259f370c-02f5-4a6c-beab-0cc3128c51ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89de584b-c3f3-4bfc-88c5-91513f2b698d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}