{
    "id": "53fa06b0-625b-4b74-944c-cc01168e5e8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 246,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e52ca198-5268-4d81-ac47-6b705c98f1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53fa06b0-625b-4b74-944c-cc01168e5e8d",
            "compositeImage": {
                "id": "6cd807fe-a1d9-49f8-b513-b9b6e95e8aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e52ca198-5268-4d81-ac47-6b705c98f1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd371218-1ac7-4f1e-ab6c-8c30e97aab89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e52ca198-5268-4d81-ac47-6b705c98f1f9",
                    "LayerId": "4e2243d4-8f7f-4217-8c72-cba7ab27cd47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4e2243d4-8f7f-4217-8c72-cba7ab27cd47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53fa06b0-625b-4b74-944c-cc01168e5e8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 300
}