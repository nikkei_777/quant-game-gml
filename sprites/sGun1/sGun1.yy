{
    "id": "fb8a89a3-d649-4bc4-b3a0-11c99cd8295d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b8cb317-e1c5-4333-aa61-15a5a0423fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb8a89a3-d649-4bc4-b3a0-11c99cd8295d",
            "compositeImage": {
                "id": "695af548-37bb-440f-99ff-43483417516d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8cb317-e1c5-4333-aa61-15a5a0423fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93cce504-d98c-4ff6-bd6f-5b7fdbc10c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8cb317-e1c5-4333-aa61-15a5a0423fcd",
                    "LayerId": "cdafc22d-59d6-4c14-8196-c6495d2f97e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "cdafc22d-59d6-4c14-8196-c6495d2f97e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb8a89a3-d649-4bc4-b3a0-11c99cd8295d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 6
}