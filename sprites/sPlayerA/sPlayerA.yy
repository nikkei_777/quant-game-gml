{
    "id": "2f7a82fc-b015-458e-87ee-2f085c713781",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 20,
    "bbox_right": 43,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e1d78bd-6cbf-423e-b7d7-b1c2b0426454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7a82fc-b015-458e-87ee-2f085c713781",
            "compositeImage": {
                "id": "4dd01e07-b55d-4d1f-b3d8-8eea2f011fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1d78bd-6cbf-423e-b7d7-b1c2b0426454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8dc1248-aecd-4287-8405-86cb1d9d273e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1d78bd-6cbf-423e-b7d7-b1c2b0426454",
                    "LayerId": "850765c7-92e4-495f-9502-ab10a29cead7"
                }
            ]
        },
        {
            "id": "5453c28e-0851-4724-b646-b3da832b64c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7a82fc-b015-458e-87ee-2f085c713781",
            "compositeImage": {
                "id": "bc620ded-74b2-4ddc-a540-a3f0bdf73cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5453c28e-0851-4724-b646-b3da832b64c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c71ec3-e3aa-4c05-8f94-a5bf959224e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5453c28e-0851-4724-b646-b3da832b64c5",
                    "LayerId": "850765c7-92e4-495f-9502-ab10a29cead7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "850765c7-92e4-495f-9502-ab10a29cead7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f7a82fc-b015-458e-87ee-2f085c713781",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}