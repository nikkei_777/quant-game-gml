{
    "id": "462c526c-b261-4c1a-8355-c3494ec14359",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 15,
    "bbox_right": 46,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce0927af-9876-44bd-9267-e18ac9448f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "462c526c-b261-4c1a-8355-c3494ec14359",
            "compositeImage": {
                "id": "3383dfb3-bc51-4f9f-b019-564171b3a36e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce0927af-9876-44bd-9267-e18ac9448f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89fbe32e-7375-4942-88c4-d6ac613c1288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce0927af-9876-44bd-9267-e18ac9448f9f",
                    "LayerId": "140a6488-2ee1-4e76-9b0c-16ccd3b4d549"
                }
            ]
        },
        {
            "id": "bd8a4cd6-7066-41a0-b4fd-cb832ce429e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "462c526c-b261-4c1a-8355-c3494ec14359",
            "compositeImage": {
                "id": "c3b07f5b-fe88-482a-9de4-55b7a3a27d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8a4cd6-7066-41a0-b4fd-cb832ce429e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8fa9ea-249e-47b9-ad0a-0a686137beac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8a4cd6-7066-41a0-b4fd-cb832ce429e3",
                    "LayerId": "140a6488-2ee1-4e76-9b0c-16ccd3b4d549"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "140a6488-2ee1-4e76-9b0c-16ccd3b4d549",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "462c526c-b261-4c1a-8355-c3494ec14359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}