{
    "id": "3899f003-1820-4556-9082-5001197e9cb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRb1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2173f122-e1e9-4542-ae0e-36774fb3ad48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3899f003-1820-4556-9082-5001197e9cb6",
            "compositeImage": {
                "id": "864ace0c-c3ca-442e-8caf-a4cf95268f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2173f122-e1e9-4542-ae0e-36774fb3ad48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f97c53-749b-463a-ac72-c78ad7923db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2173f122-e1e9-4542-ae0e-36774fb3ad48",
                    "LayerId": "e359808a-0616-42fd-bc0b-dc9874635d47"
                }
            ]
        },
        {
            "id": "fdb59431-b470-4a68-8384-956d8790cfe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3899f003-1820-4556-9082-5001197e9cb6",
            "compositeImage": {
                "id": "be8eb74b-abf5-4c4d-adb2-a881bd43edf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb59431-b470-4a68-8384-956d8790cfe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4fadc0-a658-47cc-90f7-005e0a5c8d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb59431-b470-4a68-8384-956d8790cfe5",
                    "LayerId": "e359808a-0616-42fd-bc0b-dc9874635d47"
                }
            ]
        },
        {
            "id": "a1756f97-2164-492e-bc15-7c57b75335b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3899f003-1820-4556-9082-5001197e9cb6",
            "compositeImage": {
                "id": "14942ca6-f320-4884-901d-35824e1ff937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1756f97-2164-492e-bc15-7c57b75335b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e0e889-c7dc-4c51-8bba-bab66ad7a92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1756f97-2164-492e-bc15-7c57b75335b9",
                    "LayerId": "e359808a-0616-42fd-bc0b-dc9874635d47"
                }
            ]
        },
        {
            "id": "e00e5349-63db-4e70-993c-3c819ef01107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3899f003-1820-4556-9082-5001197e9cb6",
            "compositeImage": {
                "id": "8db14792-dd36-4be3-b95c-d41bf8123dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00e5349-63db-4e70-993c-3c819ef01107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b18945c-fa83-410d-84be-fdfce7ae1b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00e5349-63db-4e70-993c-3c819ef01107",
                    "LayerId": "e359808a-0616-42fd-bc0b-dc9874635d47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e359808a-0616-42fd-bc0b-dc9874635d47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3899f003-1820-4556-9082-5001197e9cb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}