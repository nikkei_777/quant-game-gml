{
    "id": "998ca0fe-4222-443a-b640-40816f6cef0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55992d8f-18ff-4f26-950b-005732d7d5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "998ca0fe-4222-443a-b640-40816f6cef0b",
            "compositeImage": {
                "id": "9a225fdb-1395-4d9f-b35f-d0401c506dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55992d8f-18ff-4f26-950b-005732d7d5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3cbc4ae-0b66-4d70-9b67-d62113f9bdb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55992d8f-18ff-4f26-950b-005732d7d5fc",
                    "LayerId": "293e1d2c-6365-40b4-8379-1f2b33fcf560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "293e1d2c-6365-40b4-8379-1f2b33fcf560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "998ca0fe-4222-443a-b640-40816f6cef0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": -1
}