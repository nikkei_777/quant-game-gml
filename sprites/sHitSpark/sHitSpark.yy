{
    "id": "9422c08d-bba2-4128-b482-43ee5686a379",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHitSpark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d90259ac-1219-4595-92c8-0088c7c7f706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "85c87e15-298a-4065-a862-39581f900f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90259ac-1219-4595-92c8-0088c7c7f706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c881c80d-6b48-4ec0-8df2-a1469d11f6a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90259ac-1219-4595-92c8-0088c7c7f706",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        },
        {
            "id": "675bb97a-9981-4cc4-b6c2-115ed31bcf14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "a87c1903-3ab8-42a8-8e89-85341b8c41d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675bb97a-9981-4cc4-b6c2-115ed31bcf14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27abec12-3920-4da8-9ec7-5c28bf04fb7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675bb97a-9981-4cc4-b6c2-115ed31bcf14",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        },
        {
            "id": "fff2bad2-6cf2-4ed9-a7d4-d464c6c4acd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "1d280844-d608-4b92-8f4f-c3a2aa786c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff2bad2-6cf2-4ed9-a7d4-d464c6c4acd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a267d086-d58f-4469-93c3-35e1fa398e79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff2bad2-6cf2-4ed9-a7d4-d464c6c4acd9",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        },
        {
            "id": "84c7fb82-03ac-494b-83b9-b417aa09817e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "28c609ae-690e-4588-ba7b-e89cbd9eb99a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c7fb82-03ac-494b-83b9-b417aa09817e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7336b978-e7f8-4b4c-9ac9-69b9b2a5f02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c7fb82-03ac-494b-83b9-b417aa09817e",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        },
        {
            "id": "485e4d18-68ed-4cb4-95b9-165018741b91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "2e741847-27b9-46e5-b5f0-3210cd35f211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485e4d18-68ed-4cb4-95b9-165018741b91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "000e4758-5222-4732-b7ac-6c9f13733744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485e4d18-68ed-4cb4-95b9-165018741b91",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        },
        {
            "id": "016cef69-08f6-463c-bbd2-a39de2c5b81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "compositeImage": {
                "id": "d020413f-d00e-4d97-804c-ee761f3f4445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016cef69-08f6-463c-bbd2-a39de2c5b81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cfcdfd-6379-4965-913d-c4dfdfdf6212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016cef69-08f6-463c-bbd2-a39de2c5b81b",
                    "LayerId": "baa7feea-1194-4efb-ba9e-d9fb633aba5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "baa7feea-1194-4efb-ba9e-d9fb633aba5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 18
}