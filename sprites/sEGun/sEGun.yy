{
    "id": "7c3b4945-a4d4-447c-8291-ee8ece09e4bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40c6ecb4-58eb-4d63-96f6-2de359f77676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c3b4945-a4d4-447c-8291-ee8ece09e4bd",
            "compositeImage": {
                "id": "38314dc5-7da1-422e-a477-885daf773439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c6ecb4-58eb-4d63-96f6-2de359f77676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a26f585-f0b6-44c6-a8d6-765037800a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c6ecb4-58eb-4d63-96f6-2de359f77676",
                    "LayerId": "c0712c46-a014-4551-b0e2-97198ac0013f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "c0712c46-a014-4551-b0e2-97198ac0013f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c3b4945-a4d4-447c-8291-ee8ece09e4bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}