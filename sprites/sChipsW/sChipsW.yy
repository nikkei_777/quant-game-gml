{
    "id": "48db4aab-e188-44ae-a18c-7e454a618c6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChipsW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b0993b3-9b89-4e7e-95f9-6051f2666946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "compositeImage": {
                "id": "092ab028-ba67-43f8-abb1-e92465ccd72d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b0993b3-9b89-4e7e-95f9-6051f2666946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbde10ca-bf6b-41f9-a002-bc7eae0dd2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b0993b3-9b89-4e7e-95f9-6051f2666946",
                    "LayerId": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff"
                }
            ]
        },
        {
            "id": "ad9336eb-7ebf-4099-8559-4555a0a4c51e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "compositeImage": {
                "id": "1cb42eeb-3846-42bf-9242-8497d80ec18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9336eb-7ebf-4099-8559-4555a0a4c51e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9ff1092-24ef-4c7a-acde-15708436d4b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9336eb-7ebf-4099-8559-4555a0a4c51e",
                    "LayerId": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff"
                }
            ]
        },
        {
            "id": "f83197ec-bcb4-4ce6-ba2f-d420b9346f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "compositeImage": {
                "id": "bebbe45e-8671-42f1-b5a6-5fe3eb352bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83197ec-bcb4-4ce6-ba2f-d420b9346f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a6da1d-7179-4e05-a1d8-69c3d9e898b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83197ec-bcb4-4ce6-ba2f-d420b9346f42",
                    "LayerId": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff"
                }
            ]
        },
        {
            "id": "1f3bc66b-186c-4357-8046-3f83c79f4d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "compositeImage": {
                "id": "85b08c02-2ac9-4ff6-8715-6a7d59ae2637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3bc66b-186c-4357-8046-3f83c79f4d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8316f828-d61e-42c4-9d85-6e9eeba16c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3bc66b-186c-4357-8046-3f83c79f4d91",
                    "LayerId": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff"
                }
            ]
        },
        {
            "id": "a00f76f1-c316-423c-9335-805e54b5ec36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "compositeImage": {
                "id": "c45cb391-030f-4861-b85d-1b470e4fd32b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00f76f1-c316-423c-9335-805e54b5ec36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd6c7d5-b475-4e23-ae6c-8128e2fb060f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00f76f1-c316-423c-9335-805e54b5ec36",
                    "LayerId": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "adbdcfff-8d5b-4da7-b949-8bc21f9cd3ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}