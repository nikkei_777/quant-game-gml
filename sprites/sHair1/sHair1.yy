{
    "id": "ef7e3368-7fe1-4405-9334-8f6ea0cd75ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHair1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e9fe6fd-5481-4700-b94b-bd8b7727f539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7e3368-7fe1-4405-9334-8f6ea0cd75ce",
            "compositeImage": {
                "id": "a2699180-6f95-4307-a2c2-7c0b3972aab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9fe6fd-5481-4700-b94b-bd8b7727f539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9266d66-ffc7-42b9-a480-38af70ebd167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9fe6fd-5481-4700-b94b-bd8b7727f539",
                    "LayerId": "ac486bb5-7ee9-488a-aff0-bd76f30f8afa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ac486bb5-7ee9-488a-aff0-bd76f30f8afa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef7e3368-7fe1-4405-9334-8f6ea0cd75ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}