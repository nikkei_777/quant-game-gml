{
    "id": "8d91bc1a-e5d7-404d-a7fa-56d68836b36d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCrate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13d0bdbf-25b7-45fd-8f48-5c38b3d8f1e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d91bc1a-e5d7-404d-a7fa-56d68836b36d",
            "compositeImage": {
                "id": "dae858c3-270a-460a-914b-e60944b6a438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d0bdbf-25b7-45fd-8f48-5c38b3d8f1e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8cfd898-59a0-4b47-ba54-d71ad50e8b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d0bdbf-25b7-45fd-8f48-5c38b3d8f1e3",
                    "LayerId": "40956b39-5620-4c69-a395-59748a5b99a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40956b39-5620-4c69-a395-59748a5b99a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d91bc1a-e5d7-404d-a7fa-56d68836b36d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}