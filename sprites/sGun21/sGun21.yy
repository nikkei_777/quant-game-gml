{
    "id": "ba767a2c-fa75-4f05-9d94-d07632d11721",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e941bfaa-31bb-419b-a1af-d1083d12bff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba767a2c-fa75-4f05-9d94-d07632d11721",
            "compositeImage": {
                "id": "03d2686a-ca38-46e2-88dd-4b6bf377dac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e941bfaa-31bb-419b-a1af-d1083d12bff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd4b5b3-b715-447c-8f0d-6970741862cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e941bfaa-31bb-419b-a1af-d1083d12bff7",
                    "LayerId": "222061c8-30d0-4732-9248-badd3f3e7758"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "222061c8-30d0-4732-9248-badd3f3e7758",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba767a2c-fa75-4f05-9d94-d07632d11721",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 15
}