{
    "id": "dd5a5d43-9194-4ead-95ee-da16cc03f583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0b02059-9943-4b63-9ad2-9c10d3b1ec36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd5a5d43-9194-4ead-95ee-da16cc03f583",
            "compositeImage": {
                "id": "92f56d54-6b8e-4204-8b23-c7fc2a012f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b02059-9943-4b63-9ad2-9c10d3b1ec36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd73afec-fbe9-4b62-9f3d-4945006c58e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b02059-9943-4b63-9ad2-9c10d3b1ec36",
                    "LayerId": "db2fee3d-5307-42d6-98fd-b139a09606a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "db2fee3d-5307-42d6-98fd-b139a09606a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd5a5d43-9194-4ead-95ee-da16cc03f583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 15
}