{
    "id": "13dd0aee-af59-45cb-ae54-6507e9716940",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 298,
    "bbox_left": 9,
    "bbox_right": 514,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af609288-d487-4935-91fe-2e26f8ed0131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13dd0aee-af59-45cb-ae54-6507e9716940",
            "compositeImage": {
                "id": "694c446f-3412-4e6c-9412-c4d77d7ae2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af609288-d487-4935-91fe-2e26f8ed0131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d9e4b7-70e5-44e4-87ae-824efae1228e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af609288-d487-4935-91fe-2e26f8ed0131",
                    "LayerId": "0d335bd1-07d2-45f7-9410-3291873ccf5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "0d335bd1-07d2-45f7-9410-3291873ccf5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13dd0aee-af59-45cb-ae54-6507e9716940",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}