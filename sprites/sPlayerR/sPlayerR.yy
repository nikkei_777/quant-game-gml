{
    "id": "37c983f6-a5d3-4798-bf46-59a36906190f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 21,
    "bbox_right": 42,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcd53bdd-e9aa-4ede-a492-73c46dcefe44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c983f6-a5d3-4798-bf46-59a36906190f",
            "compositeImage": {
                "id": "af77fcb1-e719-43a6-8821-b9309bc9c36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd53bdd-e9aa-4ede-a492-73c46dcefe44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c896bbe-120f-48e8-9e3f-79927ef7db3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd53bdd-e9aa-4ede-a492-73c46dcefe44",
                    "LayerId": "8b343c33-395b-4a70-b173-c49b935cb0a0"
                }
            ]
        },
        {
            "id": "5576598e-e322-43fe-8eb9-8ce9fdb19b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c983f6-a5d3-4798-bf46-59a36906190f",
            "compositeImage": {
                "id": "67aa63fc-821d-4394-af92-19dcb876c795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5576598e-e322-43fe-8eb9-8ce9fdb19b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ca861d-452c-459c-80d9-64ca8d21d69f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5576598e-e322-43fe-8eb9-8ce9fdb19b46",
                    "LayerId": "8b343c33-395b-4a70-b173-c49b935cb0a0"
                }
            ]
        },
        {
            "id": "35a5a7aa-a5af-4e18-9e98-815b7951eea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c983f6-a5d3-4798-bf46-59a36906190f",
            "compositeImage": {
                "id": "892fa4a8-10c7-4fb4-b63d-9f2d3843f57f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a5a7aa-a5af-4e18-9e98-815b7951eea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f019f0-73c4-4618-a2b7-c19863624dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a5a7aa-a5af-4e18-9e98-815b7951eea9",
                    "LayerId": "8b343c33-395b-4a70-b173-c49b935cb0a0"
                }
            ]
        },
        {
            "id": "bc4527ab-fd88-4594-87c2-1a13615c618e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c983f6-a5d3-4798-bf46-59a36906190f",
            "compositeImage": {
                "id": "edf5f1de-d92f-4b62-aa28-1c5c61708e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4527ab-fd88-4594-87c2-1a13615c618e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49718b9d-c588-443a-b541-e89f21f536ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4527ab-fd88-4594-87c2-1a13615c618e",
                    "LayerId": "8b343c33-395b-4a70-b173-c49b935cb0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8b343c33-395b-4a70-b173-c49b935cb0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37c983f6-a5d3-4798-bf46-59a36906190f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}