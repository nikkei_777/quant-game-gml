{
    "id": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7af65186-7476-467d-8675-3cd25c939c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
            "compositeImage": {
                "id": "465e0b02-657c-4636-933f-7e955e11c5a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7af65186-7476-467d-8675-3cd25c939c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9566849-dc81-473f-ad34-7b097b06621d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7af65186-7476-467d-8675-3cd25c939c95",
                    "LayerId": "b4c8ef68-5857-4f50-8d61-c7ffc2dc2726"
                }
            ]
        },
        {
            "id": "374d9322-83a3-429e-9ce9-0642fd42111a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
            "compositeImage": {
                "id": "f5a50ffe-3754-4f3f-94ff-0c0df9407e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374d9322-83a3-429e-9ce9-0642fd42111a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee5f6b9-ac23-49a4-870b-0d568a21ff2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374d9322-83a3-429e-9ce9-0642fd42111a",
                    "LayerId": "b4c8ef68-5857-4f50-8d61-c7ffc2dc2726"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b4c8ef68-5857-4f50-8d61-c7ffc2dc2726",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}