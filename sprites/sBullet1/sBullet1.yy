{
    "id": "f2ad1d8e-9eda-4bf9-89b4-e7d04b393c6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 10,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfa197b8-9d86-4821-89dc-870c825d9242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ad1d8e-9eda-4bf9-89b4-e7d04b393c6f",
            "compositeImage": {
                "id": "9552cc0b-a5d5-496e-bf2a-a8bbcfc90c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfa197b8-9d86-4821-89dc-870c825d9242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93cbc5f2-eb5f-4577-a154-629600f91f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfa197b8-9d86-4821-89dc-870c825d9242",
                    "LayerId": "f7599556-cd83-41a7-af86-db3debf8f19d"
                }
            ]
        },
        {
            "id": "4f3963d4-4864-4404-a8cf-ee6c886dda3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ad1d8e-9eda-4bf9-89b4-e7d04b393c6f",
            "compositeImage": {
                "id": "4e93970a-c48c-4739-acc7-4abb3047bcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f3963d4-4864-4404-a8cf-ee6c886dda3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72769de2-a537-46ee-b79c-79f9aaf1cf6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f3963d4-4864-4404-a8cf-ee6c886dda3f",
                    "LayerId": "f7599556-cd83-41a7-af86-db3debf8f19d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "f7599556-cd83-41a7-af86-db3debf8f19d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2ad1d8e-9eda-4bf9-89b4-e7d04b393c6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 10
}