{
    "id": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb472cc4-e4f3-498d-87db-394970bd05f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
            "compositeImage": {
                "id": "4b51a122-caef-4d0f-b0aa-0eae068aac5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb472cc4-e4f3-498d-87db-394970bd05f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f407468-56ce-48cd-a56d-d4222970eb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb472cc4-e4f3-498d-87db-394970bd05f1",
                    "LayerId": "b424d97f-4ad1-4629-b344-5307d6218968"
                }
            ]
        },
        {
            "id": "67afc97e-05c5-479b-918a-a9e75be54a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
            "compositeImage": {
                "id": "3f694739-d2fe-494a-b333-18c675398d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67afc97e-05c5-479b-918a-a9e75be54a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5eb98b-2d98-4c84-b99f-3ac27863819a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67afc97e-05c5-479b-918a-a9e75be54a1f",
                    "LayerId": "b424d97f-4ad1-4629-b344-5307d6218968"
                }
            ]
        },
        {
            "id": "de55d451-da12-4f03-9b38-0587154ee23f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
            "compositeImage": {
                "id": "1ce86c6f-5c0f-48fb-a415-2c48804c1dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de55d451-da12-4f03-9b38-0587154ee23f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3ae955-afe3-4d70-8152-50f8c4b1ee2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de55d451-da12-4f03-9b38-0587154ee23f",
                    "LayerId": "b424d97f-4ad1-4629-b344-5307d6218968"
                }
            ]
        },
        {
            "id": "a06838d4-a32d-4da8-ab73-199996d049ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
            "compositeImage": {
                "id": "32969f0f-295a-419a-8a29-1ced4e1e1895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06838d4-a32d-4da8-ab73-199996d049ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2cbe25-32d4-4197-a061-84cd0e344e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06838d4-a32d-4da8-ab73-199996d049ae",
                    "LayerId": "b424d97f-4ad1-4629-b344-5307d6218968"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b424d97f-4ad1-4629-b344-5307d6218968",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edda4e78-b6ae-46e7-83a9-b8f464d1f4df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}