{
    "id": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fef2a14d-a4f3-4560-87b0-a9da68b22322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "d51269b7-7702-4117-b86b-4c0e051c51da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef2a14d-a4f3-4560-87b0-a9da68b22322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "178cc772-402b-4bb8-8fb9-a35ef71aa1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef2a14d-a4f3-4560-87b0-a9da68b22322",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "db254019-0c34-4e82-b5eb-e7f45b2eaffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "ee4cceed-2b64-4def-be55-cf97054c1739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db254019-0c34-4e82-b5eb-e7f45b2eaffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafb5de8-f9fc-4a97-8770-99af45071689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db254019-0c34-4e82-b5eb-e7f45b2eaffc",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "0ce4d1e2-4355-406a-b608-dd9b6719dc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "3dd1cede-0d0f-4c16-a27c-f63645b1acd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce4d1e2-4355-406a-b608-dd9b6719dc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce91406-1d48-4361-81e5-50108025351e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce4d1e2-4355-406a-b608-dd9b6719dc7a",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "68b15dc4-e0ab-4d67-932c-b7bc6bcaa2f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "a32ae89e-b09f-4609-9859-345aba4b2806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b15dc4-e0ab-4d67-932c-b7bc6bcaa2f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d2cd5d-1576-49ab-b1f3-f4c8b16f57e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b15dc4-e0ab-4d67-932c-b7bc6bcaa2f8",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "94a0cca1-ec6f-4524-b568-30a50eec1d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "0121d587-a967-4678-9147-f4fba25d5e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a0cca1-ec6f-4524-b568-30a50eec1d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01cff346-9305-4328-94e6-3a60d5ea6d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a0cca1-ec6f-4524-b568-30a50eec1d0b",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "28b94c03-3d56-4707-9130-cbf0c2cb6a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "a71a112b-32a0-402d-b2b6-fd8ad0a3523f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b94c03-3d56-4707-9130-cbf0c2cb6a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9cbdb1-0e39-4e80-8fd4-513718021887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b94c03-3d56-4707-9130-cbf0c2cb6a72",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "b97898b5-8896-4167-a924-c8c3b19a3742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "f1683acb-1fa1-4552-959a-e8161d1d4dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97898b5-8896-4167-a924-c8c3b19a3742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f8296f-39c3-4a51-93a1-8bcf9bdc904a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97898b5-8896-4167-a924-c8c3b19a3742",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        },
        {
            "id": "f7df6ef9-5948-46fc-9eff-ee36c783c903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "compositeImage": {
                "id": "821676d7-7415-4c0a-90e3-7304b606a764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7df6ef9-5948-46fc-9eff-ee36c783c903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f32ed773-b83a-4280-a521-36e0030e5be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7df6ef9-5948-46fc-9eff-ee36c783c903",
                    "LayerId": "f34542e2-fe14-49c1-b086-d7894deba4b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f34542e2-fe14-49c1-b086-d7894deba4b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}