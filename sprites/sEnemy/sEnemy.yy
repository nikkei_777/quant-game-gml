{
    "id": "88027b21-d010-4337-b3db-6a3437864b20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eda417ab-a87d-4df7-8a97-704e1abd047f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88027b21-d010-4337-b3db-6a3437864b20",
            "compositeImage": {
                "id": "fcc69f38-a3e3-4cc1-8d7a-57f0ac162f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda417ab-a87d-4df7-8a97-704e1abd047f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe7b552-bb89-46b2-9e5a-e9c26693d438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda417ab-a87d-4df7-8a97-704e1abd047f",
                    "LayerId": "96c558c3-1878-4671-821a-bb85b46fb57a"
                }
            ]
        },
        {
            "id": "088b5b34-7ac6-430b-8c09-33af4f845e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88027b21-d010-4337-b3db-6a3437864b20",
            "compositeImage": {
                "id": "e32bc026-9f0e-42e7-a658-8282501cb9f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088b5b34-7ac6-430b-8c09-33af4f845e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b747bad8-7b51-42c1-90e3-90ffa0c7f4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088b5b34-7ac6-430b-8c09-33af4f845e5c",
                    "LayerId": "96c558c3-1878-4671-821a-bb85b46fb57a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "96c558c3-1878-4671-821a-bb85b46fb57a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88027b21-d010-4337-b3db-6a3437864b20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}