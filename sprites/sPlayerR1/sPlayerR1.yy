{
    "id": "991417a4-d131-49b4-8f74-21c8708d662e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 21,
    "bbox_right": 42,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22df426e-79d6-48d7-9dcf-02d8c3bf25b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "991417a4-d131-49b4-8f74-21c8708d662e",
            "compositeImage": {
                "id": "de3ed4cd-d5c4-4c32-9bc9-1a471baf0e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22df426e-79d6-48d7-9dcf-02d8c3bf25b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4251ab59-be82-46f7-b7e4-e4e9ffdd8673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22df426e-79d6-48d7-9dcf-02d8c3bf25b2",
                    "LayerId": "e50b80ae-c929-48ed-8c9b-3dd2c67f6662"
                }
            ]
        },
        {
            "id": "d1624dda-ace1-4e27-90be-4b9c010e8f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "991417a4-d131-49b4-8f74-21c8708d662e",
            "compositeImage": {
                "id": "340a4c36-1554-42d4-92b5-f0f715b71fcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1624dda-ace1-4e27-90be-4b9c010e8f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e5e28c-75dd-4bb1-bdc3-296f18ea9cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1624dda-ace1-4e27-90be-4b9c010e8f33",
                    "LayerId": "e50b80ae-c929-48ed-8c9b-3dd2c67f6662"
                }
            ]
        },
        {
            "id": "d266e77f-2237-444c-9d65-d849675c412a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "991417a4-d131-49b4-8f74-21c8708d662e",
            "compositeImage": {
                "id": "43ab2969-7094-4fdd-a0ff-2d9109577f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d266e77f-2237-444c-9d65-d849675c412a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a6cedf-1a25-444b-85c5-5694c9fd663f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d266e77f-2237-444c-9d65-d849675c412a",
                    "LayerId": "e50b80ae-c929-48ed-8c9b-3dd2c67f6662"
                }
            ]
        },
        {
            "id": "7a1b4f6d-00e9-41b9-b722-06c21593f7ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "991417a4-d131-49b4-8f74-21c8708d662e",
            "compositeImage": {
                "id": "058cb4ce-cefc-4940-888a-56fe233b0925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1b4f6d-00e9-41b9-b722-06c21593f7ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06468fb4-4073-4ed0-801c-917be76f2e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1b4f6d-00e9-41b9-b722-06c21593f7ed",
                    "LayerId": "e50b80ae-c929-48ed-8c9b-3dd2c67f6662"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e50b80ae-c929-48ed-8c9b-3dd2c67f6662",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "991417a4-d131-49b4-8f74-21c8708d662e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}