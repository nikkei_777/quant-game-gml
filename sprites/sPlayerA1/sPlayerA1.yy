{
    "id": "ec4e3de1-dde0-4e9f-ac1e-a8aecd2b796d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 20,
    "bbox_right": 43,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebb77f02-2a49-4265-b4fe-8a9917b83181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec4e3de1-dde0-4e9f-ac1e-a8aecd2b796d",
            "compositeImage": {
                "id": "23e16100-9145-4057-baf8-ee29d7a64ca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb77f02-2a49-4265-b4fe-8a9917b83181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d73b82-08f6-430c-9392-93fd66a76486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb77f02-2a49-4265-b4fe-8a9917b83181",
                    "LayerId": "8598c26e-53ef-425e-bada-bf13c24e53ac"
                }
            ]
        },
        {
            "id": "a1ef667b-84dc-4b03-b66e-eeb0fb32ba47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec4e3de1-dde0-4e9f-ac1e-a8aecd2b796d",
            "compositeImage": {
                "id": "884b0fc0-4372-456c-b8cb-b24c4bec5569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1ef667b-84dc-4b03-b66e-eeb0fb32ba47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02bab743-300a-42f1-b3ab-eec8d3c8844d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1ef667b-84dc-4b03-b66e-eeb0fb32ba47",
                    "LayerId": "8598c26e-53ef-425e-bada-bf13c24e53ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8598c26e-53ef-425e-bada-bf13c24e53ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec4e3de1-dde0-4e9f-ac1e-a8aecd2b796d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}