{
    "id": "5c7b4113-d44d-4b50-9690-37a145025511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDirtMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2138aa0-732d-42bf-93d7-ad3ed289f6f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c7b4113-d44d-4b50-9690-37a145025511",
            "compositeImage": {
                "id": "24f6abef-c100-4344-95a5-18cc794b97dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2138aa0-732d-42bf-93d7-ad3ed289f6f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea5cc29-e348-4017-9c2c-3ddb10aea8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2138aa0-732d-42bf-93d7-ad3ed289f6f9",
                    "LayerId": "151fd536-1898-4415-bc8d-78686711db90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "151fd536-1898-4415-bc8d-78686711db90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c7b4113-d44d-4b50-9690-37a145025511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 48
}