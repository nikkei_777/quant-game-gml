{
    "id": "20ad0294-be25-4c79-a14c-0987dd6e814b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d146eb74-4b9a-45e8-93a6-fa5737e8ae35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20ad0294-be25-4c79-a14c-0987dd6e814b",
            "compositeImage": {
                "id": "a01e27f4-5827-4657-9467-6d9acc42752a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d146eb74-4b9a-45e8-93a6-fa5737e8ae35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da98446-7a71-4540-9a32-930f60aca5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d146eb74-4b9a-45e8-93a6-fa5737e8ae35",
                    "LayerId": "a4696234-bd60-42f7-b83f-3ae6db521af8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a4696234-bd60-42f7-b83f-3ae6db521af8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20ad0294-be25-4c79-a14c-0987dd6e814b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 15
}