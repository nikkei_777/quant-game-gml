{
    "id": "ad73bd43-d548-433a-924e-8e299637134e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 7,
    "bbox_right": 41,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65f309aa-761d-43b8-8727-20639a40bb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad73bd43-d548-433a-924e-8e299637134e",
            "compositeImage": {
                "id": "6dcb1256-8258-4272-bec0-200f5b0e7745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f309aa-761d-43b8-8727-20639a40bb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5254ce07-9649-4779-a01b-4c95acfd35b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f309aa-761d-43b8-8727-20639a40bb45",
                    "LayerId": "d1fc9d9d-929b-4fb6-a595-d3d50c4fd1ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d1fc9d9d-929b-4fb6-a595-d3d50c4fd1ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad73bd43-d548-433a-924e-8e299637134e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}