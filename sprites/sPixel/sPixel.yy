{
    "id": "05bd16eb-9ce8-4ca4-9457-014faddb63ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ea735e0-d520-4c55-8a0b-fb70020cee6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05bd16eb-9ce8-4ca4-9457-014faddb63ca",
            "compositeImage": {
                "id": "7332081e-95cf-46d5-8594-fbf215d3b5fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea735e0-d520-4c55-8a0b-fb70020cee6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd18317-3455-4b21-b4c8-3e7a79538386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea735e0-d520-4c55-8a0b-fb70020cee6a",
                    "LayerId": "fe5f8dce-21a0-4894-b3bd-adaee11ce09c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "fe5f8dce-21a0-4894-b3bd-adaee11ce09c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05bd16eb-9ce8-4ca4-9457-014faddb63ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}