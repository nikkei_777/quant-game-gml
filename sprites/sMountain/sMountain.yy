{
    "id": "ac8f39f7-2b61-4b9c-9ec0-34ca038d1999",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMountain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 694,
    "bbox_left": 5,
    "bbox_right": 790,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a452ce-4901-48d5-83a5-53932823dbad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac8f39f7-2b61-4b9c-9ec0-34ca038d1999",
            "compositeImage": {
                "id": "b39b5e70-319c-4ce3-9f45-9b331ae2c9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a452ce-4901-48d5-83a5-53932823dbad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d57303-6ce7-4c77-a7c2-321254223842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a452ce-4901-48d5-83a5-53932823dbad",
                    "LayerId": "adc1b279-13aa-46c2-a5ec-bb84b1fab08d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 695,
    "layers": [
        {
            "id": "adc1b279-13aa-46c2-a5ec-bb84b1fab08d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac8f39f7-2b61-4b9c-9ec0-34ca038d1999",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 791,
    "xorig": 395,
    "yorig": 347
}