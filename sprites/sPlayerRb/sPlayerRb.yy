{
    "id": "d4704798-66ad-4c41-9868-5f9ab1264514",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7842448f-9d03-4980-9e30-d6b943257461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4704798-66ad-4c41-9868-5f9ab1264514",
            "compositeImage": {
                "id": "4d484858-57b4-437b-8ba5-5669413fb52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7842448f-9d03-4980-9e30-d6b943257461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a125a41-2c42-41a4-a872-d204071a02e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7842448f-9d03-4980-9e30-d6b943257461",
                    "LayerId": "b148ef5d-fe3e-404c-ad2b-61f6abf13f1c"
                }
            ]
        },
        {
            "id": "2fa7fed7-559c-4d62-a241-609d340db3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4704798-66ad-4c41-9868-5f9ab1264514",
            "compositeImage": {
                "id": "1684a836-ecbf-46b1-bfdf-94fdc9af5bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa7fed7-559c-4d62-a241-609d340db3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b2f723-0962-4e4b-a605-53eaedca99c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa7fed7-559c-4d62-a241-609d340db3f2",
                    "LayerId": "b148ef5d-fe3e-404c-ad2b-61f6abf13f1c"
                }
            ]
        },
        {
            "id": "fdc2e0ca-db8f-4574-adf0-e09a1da6e710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4704798-66ad-4c41-9868-5f9ab1264514",
            "compositeImage": {
                "id": "15002661-8afe-4125-b31e-277851c36213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc2e0ca-db8f-4574-adf0-e09a1da6e710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb5dca4-15dd-4e77-9ba6-f4f432bac06f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc2e0ca-db8f-4574-adf0-e09a1da6e710",
                    "LayerId": "b148ef5d-fe3e-404c-ad2b-61f6abf13f1c"
                }
            ]
        },
        {
            "id": "48b32d6a-047d-4a8e-a57f-c5fca4d63148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4704798-66ad-4c41-9868-5f9ab1264514",
            "compositeImage": {
                "id": "c04643fa-0dba-4628-961c-31d8a8fe522f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b32d6a-047d-4a8e-a57f-c5fca4d63148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d979f19-7533-4683-8d91-fdecead2c63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b32d6a-047d-4a8e-a57f-c5fca4d63148",
                    "LayerId": "b148ef5d-fe3e-404c-ad2b-61f6abf13f1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b148ef5d-fe3e-404c-ad2b-61f6abf13f1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4704798-66ad-4c41-9868-5f9ab1264514",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}