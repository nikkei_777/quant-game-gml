{
    "id": "d6f684fc-e738-4a0f-aa1c-b811a94a349d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChips",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac955fc4-d485-43ff-b4e2-b3d0a22a6922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f684fc-e738-4a0f-aa1c-b811a94a349d",
            "compositeImage": {
                "id": "7a221b8a-b469-4495-8a19-df7d648796c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac955fc4-d485-43ff-b4e2-b3d0a22a6922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05197699-540d-4462-b4aa-06823cf69265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac955fc4-d485-43ff-b4e2-b3d0a22a6922",
                    "LayerId": "6bb14b3c-68f3-458b-870e-4dcbdafcbf38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6bb14b3c-68f3-458b-870e-4dcbdafcbf38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6f684fc-e738-4a0f-aa1c-b811a94a349d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}