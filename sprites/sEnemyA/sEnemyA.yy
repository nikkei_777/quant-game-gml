{
    "id": "c0919c18-5adf-4207-95da-b95a4b879734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d76c5e6-6d81-49c5-af9c-26b7d0c770e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0919c18-5adf-4207-95da-b95a4b879734",
            "compositeImage": {
                "id": "d9ad737d-6eb5-49bd-bb8f-d7b2f0ffe5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d76c5e6-6d81-49c5-af9c-26b7d0c770e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d63c1a-c61b-411d-be59-f8171cddc46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d76c5e6-6d81-49c5-af9c-26b7d0c770e5",
                    "LayerId": "d700157a-8708-43a4-b07a-98011661ff6d"
                }
            ]
        },
        {
            "id": "7cd40e90-6a6f-4f4b-95db-11e3bf9663c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0919c18-5adf-4207-95da-b95a4b879734",
            "compositeImage": {
                "id": "f88d07ce-0498-423b-8c6c-386143c887d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd40e90-6a6f-4f4b-95db-11e3bf9663c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66fad950-de56-4d30-904c-17eb483eccef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd40e90-6a6f-4f4b-95db-11e3bf9663c5",
                    "LayerId": "d700157a-8708-43a4-b07a-98011661ff6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d700157a-8708-43a4-b07a-98011661ff6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0919c18-5adf-4207-95da-b95a4b879734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}