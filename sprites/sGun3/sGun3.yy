{
    "id": "c94d3bff-e16a-4a42-a052-fa6878ffc9b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c261414b-3f71-4c6f-b27f-26d3af6c8d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c94d3bff-e16a-4a42-a052-fa6878ffc9b4",
            "compositeImage": {
                "id": "82ef5beb-9a8c-4eb8-a2ca-afc1b7e45e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c261414b-3f71-4c6f-b27f-26d3af6c8d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c422609-650c-4b51-bd4c-05c6f8ba896d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c261414b-3f71-4c6f-b27f-26d3af6c8d24",
                    "LayerId": "3a2e28dc-3f46-4976-82ea-6b5b8b4e1ff3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "3a2e28dc-3f46-4976-82ea-6b5b8b4e1ff3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c94d3bff-e16a-4a42-a052-fa6878ffc9b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 6
}