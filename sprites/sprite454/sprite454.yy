{
    "id": "9f300679-0875-479d-bdf1-d178e4d4a1dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite454",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16d2893a-b48f-4525-b861-a2ce01ed33b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f300679-0875-479d-bdf1-d178e4d4a1dc",
            "compositeImage": {
                "id": "7fd4d2e4-50a4-440d-a770-273faab445ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d2893a-b48f-4525-b861-a2ce01ed33b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b36acf0-bd3e-4793-b462-526b7bfa533d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d2893a-b48f-4525-b861-a2ce01ed33b5",
                    "LayerId": "28c63449-5778-4cd4-af24-9ac7e903494c"
                },
                {
                    "id": "32dfb86b-1b62-47dc-bdc8-afc734538760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d2893a-b48f-4525-b861-a2ce01ed33b5",
                    "LayerId": "7b2cccb7-6861-49ef-b9f6-3db288830f97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "28c63449-5778-4cd4-af24-9ac7e903494c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f300679-0875-479d-bdf1-d178e4d4a1dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7b2cccb7-6861-49ef-b9f6-3db288830f97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f300679-0875-479d-bdf1-d178e4d4a1dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}