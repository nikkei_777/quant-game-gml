{
    "id": "612f96e7-90d2-4117-827d-2b6ff6a8202b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoneyG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97754b36-50f5-475b-83f2-74b70d2fd161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "612f96e7-90d2-4117-827d-2b6ff6a8202b",
            "compositeImage": {
                "id": "6d843eb6-39f0-4429-a13f-32cfbacbb7d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97754b36-50f5-475b-83f2-74b70d2fd161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2908c337-6bda-400c-96c2-1abedc86094d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97754b36-50f5-475b-83f2-74b70d2fd161",
                    "LayerId": "ceb43a68-13a0-4933-a03e-cbdfc5288436"
                }
            ]
        },
        {
            "id": "368e4c8b-63d3-43a1-97f5-e05632257f4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "612f96e7-90d2-4117-827d-2b6ff6a8202b",
            "compositeImage": {
                "id": "64ac9d80-a493-4d08-9617-312f23074e50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368e4c8b-63d3-43a1-97f5-e05632257f4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "490f154f-1bbd-46f1-8148-4ab78898fdd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368e4c8b-63d3-43a1-97f5-e05632257f4a",
                    "LayerId": "ceb43a68-13a0-4933-a03e-cbdfc5288436"
                }
            ]
        },
        {
            "id": "9079943e-8d3b-44b9-92de-e3490f125a29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "612f96e7-90d2-4117-827d-2b6ff6a8202b",
            "compositeImage": {
                "id": "5c3ce175-726d-4628-8177-5b7f35950eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9079943e-8d3b-44b9-92de-e3490f125a29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740954bd-b1d9-4a47-a230-ccb95f792278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9079943e-8d3b-44b9-92de-e3490f125a29",
                    "LayerId": "ceb43a68-13a0-4933-a03e-cbdfc5288436"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ceb43a68-13a0-4933-a03e-cbdfc5288436",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "612f96e7-90d2-4117-827d-2b6ff6a8202b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}