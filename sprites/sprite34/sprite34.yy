{
    "id": "c629a35b-e88c-4ebf-97af-9cf4d31ec07c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite34",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87b12215-2179-4638-b82f-f1fa6e2989c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c629a35b-e88c-4ebf-97af-9cf4d31ec07c",
            "compositeImage": {
                "id": "b6b7f6b5-1328-4a09-91fd-fcd38d1780b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b12215-2179-4638-b82f-f1fa6e2989c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1ab1e1f-37a5-4848-9cbd-2d35e0aa1a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b12215-2179-4638-b82f-f1fa6e2989c9",
                    "LayerId": "f5b28faa-6303-4c20-85ac-29a42efaf6ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f5b28faa-6303-4c20-85ac-29a42efaf6ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c629a35b-e88c-4ebf-97af-9cf4d31ec07c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}