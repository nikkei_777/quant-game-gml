{
    "id": "aa67eff9-ab35-479a-9b71-fda782af38b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite33",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4718c950-dca0-4980-b6b1-7c11db59bcf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa67eff9-ab35-479a-9b71-fda782af38b1",
            "compositeImage": {
                "id": "fbb7262c-2b10-4e40-a97a-3b934daf31e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4718c950-dca0-4980-b6b1-7c11db59bcf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649ff032-8467-4d1f-94fc-6426e6ff7e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4718c950-dca0-4980-b6b1-7c11db59bcf8",
                    "LayerId": "6fe270c1-de57-4898-9186-599313d2ae9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6fe270c1-de57-4898-9186-599313d2ae9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa67eff9-ab35-479a-9b71-fda782af38b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}