{
    "id": "792a9253-a427-4a50-ad93-4710dfe36b36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEbullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 8,
    "bbox_right": 32,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c0776f4-2af1-467f-8c7f-b9654af36f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "792a9253-a427-4a50-ad93-4710dfe36b36",
            "compositeImage": {
                "id": "0aa26649-8062-491a-9399-c68e381912b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0776f4-2af1-467f-8c7f-b9654af36f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6eeb11-6df3-4f6b-8656-053ca0d8c5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0776f4-2af1-467f-8c7f-b9654af36f78",
                    "LayerId": "c40bf4c1-3189-4e83-b012-ab6b2ec488e1"
                }
            ]
        },
        {
            "id": "f34388e6-23de-4716-b745-eff04afdeb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "792a9253-a427-4a50-ad93-4710dfe36b36",
            "compositeImage": {
                "id": "2fa03103-79aa-4071-bd64-bad290ac5bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34388e6-23de-4716-b745-eff04afdeb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40fd4fb8-01d4-41e3-9598-58ee9de89eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34388e6-23de-4716-b745-eff04afdeb3e",
                    "LayerId": "c40bf4c1-3189-4e83-b012-ab6b2ec488e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c40bf4c1-3189-4e83-b012-ab6b2ec488e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "792a9253-a427-4a50-ad93-4710dfe36b36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}