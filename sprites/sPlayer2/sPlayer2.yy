{
    "id": "d1d4168c-b527-4348-90ee-0a5649d21ba6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d7808be-ed6c-41f0-a48a-2b43bb5bb8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d4168c-b527-4348-90ee-0a5649d21ba6",
            "compositeImage": {
                "id": "64ff6555-5700-4ce2-bdca-38bd3e0b30c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7808be-ed6c-41f0-a48a-2b43bb5bb8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d640ec2c-c42c-4c39-9109-b6c519a65cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7808be-ed6c-41f0-a48a-2b43bb5bb8d4",
                    "LayerId": "21e839a3-c091-4827-b2d8-b2a48bae24cf"
                }
            ]
        },
        {
            "id": "2646fd27-f5b2-45c3-a8e3-003bb3528805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d4168c-b527-4348-90ee-0a5649d21ba6",
            "compositeImage": {
                "id": "6900f620-e31c-48ce-ab47-37c130c5133b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2646fd27-f5b2-45c3-a8e3-003bb3528805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "685f64db-b294-4deb-80ac-4d7578661f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2646fd27-f5b2-45c3-a8e3-003bb3528805",
                    "LayerId": "21e839a3-c091-4827-b2d8-b2a48bae24cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "21e839a3-c091-4827-b2d8-b2a48bae24cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1d4168c-b527-4348-90ee-0a5649d21ba6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}