{
    "id": "23546df1-43b9-4a00-bf75-776d360a593f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cedb8292-11b8-42ad-aa15-37f59284ca84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23546df1-43b9-4a00-bf75-776d360a593f",
            "compositeImage": {
                "id": "0c2ddbc4-32bf-4cba-8ae3-0e7138328c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cedb8292-11b8-42ad-aa15-37f59284ca84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1c811ed-2740-4a5b-854c-365ef9b0ae38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedb8292-11b8-42ad-aa15-37f59284ca84",
                    "LayerId": "e964660d-731a-4a54-a47e-224e10aa8bb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "e964660d-731a-4a54-a47e-224e10aa8bb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23546df1-43b9-4a00-bf75-776d360a593f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}