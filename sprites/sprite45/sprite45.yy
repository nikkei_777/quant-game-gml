{
    "id": "70404813-7b60-468b-bd2b-46c93f41dc28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite45",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c298154-eab9-460e-b41c-76d4144d4d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70404813-7b60-468b-bd2b-46c93f41dc28",
            "compositeImage": {
                "id": "6ebbc9ae-e76b-45f4-8966-5eb6639f2d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c298154-eab9-460e-b41c-76d4144d4d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb48a22-c262-48a0-8db4-ad5d91ba05a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c298154-eab9-460e-b41c-76d4144d4d7a",
                    "LayerId": "54a7b3af-1b30-47b0-ab0e-396167e606f5"
                },
                {
                    "id": "fef8fb54-542c-419c-9cba-8bde91a32783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c298154-eab9-460e-b41c-76d4144d4d7a",
                    "LayerId": "d798cc7d-555b-4dfd-a6a9-96754c9fe095"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "d798cc7d-555b-4dfd-a6a9-96754c9fe095",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70404813-7b60-468b-bd2b-46c93f41dc28",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "54a7b3af-1b30-47b0-ab0e-396167e606f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70404813-7b60-468b-bd2b-46c93f41dc28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}