{
    "id": "4e8feff6-eea1-479a-a2ae-d9adcb781008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd66388e-59d8-430c-8168-a4d08a54261e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e8feff6-eea1-479a-a2ae-d9adcb781008",
            "compositeImage": {
                "id": "dfc2e094-58b3-4e33-9db7-5f79f38a297e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd66388e-59d8-430c-8168-a4d08a54261e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a543077-d224-4372-9be4-6c3db00c7e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd66388e-59d8-430c-8168-a4d08a54261e",
                    "LayerId": "3b356818-abb9-4412-a9ed-cac232f611ca"
                }
            ]
        },
        {
            "id": "33e11a0c-f313-47b8-8323-a516ebef8b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e8feff6-eea1-479a-a2ae-d9adcb781008",
            "compositeImage": {
                "id": "6ad30535-bff0-48a1-b7d4-6f27c810e4e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e11a0c-f313-47b8-8323-a516ebef8b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea106dd-5ddf-4316-8b93-de11d761fee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e11a0c-f313-47b8-8323-a516ebef8b99",
                    "LayerId": "3b356818-abb9-4412-a9ed-cac232f611ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b356818-abb9-4412-a9ed-cac232f611ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e8feff6-eea1-479a-a2ae-d9adcb781008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}