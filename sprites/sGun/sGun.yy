{
    "id": "1c7a880d-aec3-469f-b677-49f4c256833a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b413c05a-9ebf-40b3-927f-6b72def73c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c7a880d-aec3-469f-b677-49f4c256833a",
            "compositeImage": {
                "id": "ae0d4759-5d61-4398-988b-f86f8f05b62c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b413c05a-9ebf-40b3-927f-6b72def73c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46ec341-87d4-49a4-a86f-ddd72218f235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b413c05a-9ebf-40b3-927f-6b72def73c17",
                    "LayerId": "52863b6b-d1cb-42d8-89da-55d2769a35a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52863b6b-d1cb-42d8-89da-55d2769a35a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c7a880d-aec3-469f-b677-49f4c256833a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 15
}