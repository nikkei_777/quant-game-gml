{
    "id": "fda2ba45-573b-4fa2-bbef-8c50db5ff9d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStatue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 11,
    "bbox_right": 116,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "178ba816-5828-427a-86d9-afa9da256408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fda2ba45-573b-4fa2-bbef-8c50db5ff9d6",
            "compositeImage": {
                "id": "a2bad52e-0cdb-415b-9d28-bf735a1b2a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178ba816-5828-427a-86d9-afa9da256408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc76db1c-d577-4128-8fe9-52519a5dc86a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178ba816-5828-427a-86d9-afa9da256408",
                    "LayerId": "5b80c1c4-f4fd-4747-a8d5-3a30b716e068"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5b80c1c4-f4fd-4747-a8d5-3a30b716e068",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fda2ba45-573b-4fa2-bbef-8c50db5ff9d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}