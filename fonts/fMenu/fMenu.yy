{
    "id": "a16f9537-70bc-4b24-92bf-eed17726eb87",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Born2bSportyV2",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c1da98f9-ec43-49e5-9f4a-d45994f3885c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 54,
                "y": 150
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8bcdd5b1-f886-4eca-8083-24db26867254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 200,
                "y": 150
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f38b4b36-fb74-44eb-b857-7a60fd8dfad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 30,
                "y": 150
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "88ed7da9-e445-4782-a9a2-4c8fedf7d7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e8269254-68ba-4aed-8869-2b45bfa5d739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 76
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9f7d18b8-f1ca-44f2-8313-79f7a6ab01c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b7a73558-376e-4c89-8bef-06d3be08dd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a91908d8-de6b-44ec-828c-cc0dd58b0685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 182,
                "y": 150
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a20b6094-0cac-48f1-80d0-c09d6c0df3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 148,
                "y": 150
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7ac40c4a-1c24-48db-8cfa-51ad85a15322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 140,
                "y": 150
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bf936a18-2990-4edc-a66a-2036dddc286d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 76
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ecbfdd5a-ff31-4124-9a89-aee303408549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c7a88860-e297-491a-9899-80f4fec9e785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 218,
                "y": 150
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ebd7112c-c5f6-4942-a216-8432f1c4e2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 42,
                "y": 150
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ce8bf0a7-aaca-4dc3-808c-ba5118d61f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 164,
                "y": 150
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cc30ce9f-a241-4605-a9f5-a493148d1f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 76
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "78c79300-b629-4562-b3e9-64d15821dedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6ec6c1dd-b092-4cc2-8439-c8e18ad8a4c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 156,
                "y": 150
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "446008f5-7be2-4cfe-acdd-e624c1578b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "05ed6692-c55a-4cd0-988f-9241e55bda1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 113
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4d6cb831-ee52-49fb-8c34-03c38e903cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "938bede8-4a88-4853-8414-454fe28eca42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 113
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1b5ebaa6-e79b-4aaf-8e4f-11736ce3fc9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 113
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fc8d83a1-df76-4641-8f8f-06d3cbcf399d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 113
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2db45a9c-f97d-464f-a945-ac73ef623dbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 113
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ee932591-c916-4c82-8b4b-040f5d78c096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 113
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4c7bdf3b-d6a2-4bac-8807-171b0db93299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 170,
                "y": 150
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1bbea381-4e86-42db-819c-36164dcb6786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 212,
                "y": 150
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6c71c833-c5e9-4d53-8941-bdc4f7a063f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8d3bc0bf-cccf-47b9-a5fa-365f85a31f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 113
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1854357e-125c-41d9-af57-2dde503a48c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "da0928bb-421d-4704-b45d-1c2b03a34be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 113
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7c5b65d1-5dee-44bb-a480-2edfa5f90ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a6cea450-9cf5-439a-a28b-c9d23db9ceac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 150
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e4234a18-6ced-42c6-9e71-a2b035185759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 113
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f0782f32-e9f8-4def-a493-da02e75bd00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 113
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b710027f-6600-4cd2-8680-23f819f76792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 113
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e6ec581b-b127-476a-b9e7-ddd9d01b9efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 113
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "758381f4-24bd-4b7b-8167-89f90fbb8b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 113
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "672a8f29-023f-41aa-8238-e7e9ca582afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 113
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "56639d0f-2a0a-47bc-9cde-fd156116b912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 113
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ac3a56ed-df2b-4f79-bf17-969fc77c8f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 188,
                "y": 150
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2ac42914-9222-4497-8ec0-714b47500725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 76
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6275f0c4-32ee-44d2-9046-ff22d29e5c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 113
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a1864388-33ac-4e51-9ab1-5fcbc5cff13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 76
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c07d00c4-160f-4e88-9a25-d2ecc8f87a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "74b30268-f4fe-48c0-8b28-1ac95e894e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 39
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "64e4e5ea-0d59-47c1-a9b3-6d0cd52ae6df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 76
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d980e66d-66c0-41d6-9282-43775642d8a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "895f2863-2f0d-422e-8816-14270ce54d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 39
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b339c252-059b-4fce-8f7a-ccb08c2e0e1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "06947b44-b8c2-49f2-bc6e-9627eb33d0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 39
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "066167f5-18da-46bf-bb55-b8cdf8d73bdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 39
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bcf113e9-f57a-49cc-933b-e2882b00ece2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e1f28035-af39-48ac-94ab-6c770c2480c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5b47e447-d507-481a-9349-d6c29c621f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2183ab3e-d493-4db6-80a3-2a13e8bf16eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 39
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9e4512b4-bc19-457c-a5a8-7636d3c6b89e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4bf298ee-258c-4024-9b28-d5fcafac94c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "41b2e4db-6a9b-479c-afbc-1099d8166cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 132,
                "y": 150
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0d5cb2c4-13f3-434d-91f6-1b0b522677fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5ba74926-acb6-4fd8-a180-bf6ff0e22c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 124,
                "y": 150
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "81928984-d9ab-4c1d-80a2-4194f89715b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "29b0b7ce-b45e-4f7a-a26f-017691d58f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5ce3e188-517c-4c8d-a502-806defd558bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 150
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4ba1d8f2-e1dd-448f-88fb-f5f888a9bdf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 39
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "630851d0-0a32-4470-8310-66721dc2340d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 76
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "29eaf759-77f1-4cf3-bff7-74e1e04349d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 39
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dc2eb48d-238c-4811-8500-3b904e0d096a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 76
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "317b7f78-5f9d-4759-8c51-558e3cbc26ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 39
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "60e2aaa4-fb8e-40cc-aede-9f0fdaf4b096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 150
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f0dd9c28-9316-4514-b8fb-ab42bea27f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 76
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "866a3765-5dcd-479d-bdc2-28d1c055055a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 76
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3ce9ce70-c1b0-4322-9d31-2ea033eb8651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 176,
                "y": 150
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4227897b-70ea-4b26-bb34-3dd6b1a71155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": -2,
                "shift": 6,
                "w": 8,
                "x": 66,
                "y": 150
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "be377c06-98de-4a3e-baac-cb6e607012c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 76
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "15ca369b-bc5b-4a83-b8eb-a799e6aeba96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 194,
                "y": 150
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "91b33bc2-14ec-492e-bdb2-05312f2f5b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6ab0e616-8b79-4f73-a958-521b04c8413e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 76
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4fb28f22-be43-4e7c-93ec-8c4e0da32de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "257a7399-cd15-48e3-8611-e8b5a460d356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 76
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4916c51d-8f74-4fce-b9f7-8bf83c8fb5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bb1f2b85-875d-4e6d-a96e-f7314a20e7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e6add9d9-4ab1-4d56-9090-59ad11cc2d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 39
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "61e86980-5da7-459e-b2ba-6af28b81db09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 150
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9244af94-45a2-4bb8-8ef1-ba2a97119670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 226,
                "y": 39
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c5a54fd0-5a47-4a8f-b263-8f191eebacb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7e208520-69d2-4335-a024-7f95d6e9a399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8646d731-5ddd-42a8-806e-dd5d5ae5836d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9951f890-07e1-4d92-b58a-9c4b531cd5b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9e112db8-e267-464d-8bc0-94b7f96fe191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 39
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d5305d5b-0597-4813-9496-6761be61acaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 76,
                "y": 150
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9f8f00c7-75d9-4650-813b-1923f3d2ca6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 206,
                "y": 150
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fde16dd0-358b-4ff4-a3fb-223c8ba277b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 96,
                "y": 150
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "41521b05-a863-4590-bcbd-d42858bd44dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 2
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}