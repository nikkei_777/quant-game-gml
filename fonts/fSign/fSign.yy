{
    "id": "9d1926e3-4fa5-4195-ba9d-89d9ad63b9ab",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fSign",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "m5x7",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5401fea8-aa56-4cf7-90e0-f70c25946f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "30c02f4f-637b-497a-8d70-87b81c991611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 10,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6ad52b9c-592c-44ce-bbf9-f640437e6981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 130,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9a96af7e-3a24-440d-ad84-b11ed58de0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2d441bdd-9e4a-4a09-969b-27ece576250e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "beb6345e-07c1-4fcd-a3d3-6af49396a185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a8369433-9b89-48f8-9f07-6d80219bb161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2510edf9-2b7a-49e0-af5f-d6bd876f54d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 248,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c838797b-018f-4bf3-932d-e154f9dbff31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 212,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "db3b9697-745c-4ccb-8d68-2b5028b92026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 206,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5fbc7bca-647b-44be-8524-e0dcfa6598f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 154,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "81873b2c-ccd7-4c71-b28c-26f347fede9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "348396c3-ebc6-404d-90d8-2f1d4f2fd7ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 236,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb02ca17-fc56-4d99-9204-76a69135ee99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3147c8fd-93f3-4f1f-aab3-78df58f232a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 6,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4e97b247-c565-47b8-b6cc-201dcd9827d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 186,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a429c1a6-42eb-4e27-afd7-3e4ecc889b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a33ca092-b766-4c9a-9c4f-86f03e7252d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cda8d804-7f66-4b10-ba92-f9863d8f1ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "403f71e5-3999-4367-8f14-8a4ee5fc5e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c62c1579-1fed-480e-9009-d97b925a7629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "82ba5d3f-49f5-4001-88d9-6cd22fd6179b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "15fc3450-fd8d-466e-ab71-fde755bbc59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d47c320b-3f31-4360-a9aa-a1b23812e4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b7fdf879-a1c4-43f9-ad11-3fa5ffe376c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f852e0d6-5847-4ead-ac3b-d5a4dcdce543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "51ce36d2-afee-4e06-90e7-bad5133de06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a7a39547-638d-4c05-9f29-c41b58874bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 242,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1a5e5364-4e3a-46f7-bd23-a1038b5d4ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 162,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "562276af-ba43-49a0-a952-481ad69bfaf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "000a19dc-9236-4ebb-bad4-30d66823cf45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 170,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d7da244f-6f75-487a-885c-11cb3e1dc8f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f81c8fdb-df60-46b2-8947-e2f25d0d819f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ebd8f266-a63a-4a69-b594-70c91e1e4c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bec007c7-81d4-42ea-8e4d-0a8e2028186b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c4d49ba2-6099-481b-bb70-6263c3997c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f53961b9-ddde-4b03-86f5-69f1a0ebce8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f7237d05-f52f-4d5f-834e-5fdec3794589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c53f2006-aefa-4b9c-8363-5fb9ce8941a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d3fb1a6d-88fa-4eb8-af45-0eb5e2d8fb62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "15e5c4bc-7338-4a60-ac30-16108a04cacb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 32
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2dc366c7-741d-42af-bdec-06aa9c151c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 32
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e07208a4-4f57-4904-8366-e353ee5a1ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6fa63edd-7e80-49b1-8d2b-31198ff14826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3149bd93-47fb-42d6-b4e8-7ecbaf6b1a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9dc559cd-cba3-4267-880e-3ebaea280c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1f6cce63-41de-4364-bba4-eed94cf1c909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4537952c-5e02-47bf-918d-77f90d59600c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "be6c869f-0e58-487c-927a-2b2cff904559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "976552bc-39d5-426f-b8b6-21f79bcc5dbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7e45bd37-f4cd-43db-a51a-3afc832420e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "38334732-5025-4abf-91fa-c927c708fe47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c063e25f-9482-4b9c-9b7d-0205390fc6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d5883fc2-71f1-4562-92d5-47d232b314b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bb6dcace-0c71-479e-a0bc-171c3249cf84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "332fad2a-7834-4c33-8ef3-af6607ffbe2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "68b7e58a-bf91-496e-8d9b-847e520b7340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d24aade8-c2d8-4e24-9aa5-ddf433e135c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c5a0efa6-069a-43fd-bca5-0f6a6d1f5fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4836820f-3bbd-4ed9-9509-95add093d51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 230,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "02559491-a707-4ffa-bdeb-02f4c4270e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 138,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "abee43d4-fca0-4081-9437-7c0c9cdf78c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 218,
                "y": 92
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ac4f0265-e3d7-4602-af05-e6c85bdd098f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "32939404-5708-4249-bfb5-eb5dab9fe452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 32
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "26a747ab-7054-4584-b879-941bb68d348d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 194,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4b064689-e8ac-4f0f-aaf5-1368d145db99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1de5247c-1871-4ee7-8cf2-6ee069329b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a3d8796d-b1cd-45a1-a4a8-03696397ae9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 92
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "993cde9c-bd63-4836-8be1-0e14d1c5ec5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 32
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ebc75d1f-3188-4932-a7bb-1355f9be53f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 32
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ea72caa7-1cf6-4036-a688-d4cb451ee98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5d8967eb-5096-4a19-8e46-c6cfa8fdec6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "33dda052-e401-480c-8ba7-3428bec0288e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b2b89de8-cc19-4a66-95a1-686a7de6770d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 224,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2bf81f5d-63b7-4c52-b89b-793bbfc80219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 178,
                "y": 92
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b49448b8-b4fd-405e-9f25-b170cc35f506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0511cd6f-4bac-485d-bdb9-405f1120e378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 200,
                "y": 92
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e09035b4-5c49-4474-86c7-c8e8b692defd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "03671f30-e938-4b42-b61a-37e0b9bb074b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6a120bd3-7a26-443a-b7c2-b90234d934a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "127753ef-afa2-4db9-8de1-b34bd9053f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "354bd530-05b2-4bc3-8292-a729fcae3107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9a4ef59b-8d6c-40c3-8540-e668e58e2918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 92
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fa47b14b-0215-401e-ac6d-0453e94423d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2876b585-3df5-4c0d-b045-c65d653942ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f0943c9e-a7a6-4b13-81e6-f39f5373ec88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "47b1b5fd-16d4-4f0b-8602-ce69d28a1558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "91439db6-cb10-4552-afea-76de2903e668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "afb1d744-342b-4164-b701-5d727df75dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5cc15fb1-50ba-475b-af56-9eaca0672e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b0cc0bf7-29e1-4f45-81ac-a36b8cd1b3d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "37641862-fa2b-4f72-bd4a-d7aa12ace626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 122,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ee86a028-82c3-431f-b8d5-4225cf1082f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 14,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f2dd38e2-6f31-4ce3-b736-7c644e568c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 146,
                "y": 92
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d9a121a2-24d4-479a-abe9-6842a622aada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}