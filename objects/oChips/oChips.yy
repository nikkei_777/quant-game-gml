{
    "id": "4d7c9f3b-a537-40dc-8819-8f269cfe8ea0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChips",
    "eventList": [
        {
            "id": "14aac7a4-e00b-4769-b17e-7d4ec437564d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4d7c9f3b-a537-40dc-8819-8f269cfe8ea0"
        },
        {
            "id": "69ee1aa6-19b4-4889-8a06-0bb8cac9fcaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d7c9f3b-a537-40dc-8819-8f269cfe8ea0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6f684fc-e738-4a0f-aa1c-b811a94a349d",
    "visible": true
}