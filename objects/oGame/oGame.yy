{
    "id": "b54818b1-ab4f-47e2-b917-1f586cb5c946",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGame",
    "eventList": [
        {
            "id": "800a5a70-0483-4e66-9143-637f08adca53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 9,
            "m_owner": "b54818b1-ab4f-47e2-b917-1f586cb5c946"
        },
        {
            "id": "1c419efc-ef4f-4575-b060-301774496a69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b54818b1-ab4f-47e2-b917-1f586cb5c946"
        },
        {
            "id": "e354e79b-4a6b-475e-91e7-314b822f89d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b54818b1-ab4f-47e2-b917-1f586cb5c946"
        },
        {
            "id": "79f068d6-c240-4387-b6d6-fc2bfebcd337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b54818b1-ab4f-47e2-b917-1f586cb5c946"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}