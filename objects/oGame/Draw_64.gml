/// @desc Draw Score
if (room != rMenu) && (instance_exists(oPlayer)) 
{
	killtextscale = max(killtextscale * 0.95, 1);
	DrawSetText(c_black, fMenu, fa_right, fa_top);
	draw_text_transformed(RES_W-8,38,string(global.q) + " Q-coins:", killtextscale,killtextscale,0);
	draw_set_colour(c_white);
	draw_text_transformed(RES_W-10,36,string(global.q) + " Q-coins:", killtextscale,killtextscale,0);
}

if (room != rMenu) && (instance_exists(oPlayer)) 
{
	killtextscale = max(killtextscale * 0.95, 1);
	DrawSetText(c_black, fMenu, fa_right, fa_top);
	draw_text_transformed(RES_W-8,12,string(global.hpp) + " Health:", killtextscale,killtextscale,0);
	draw_set_colour(c_white);
	draw_text_transformed(RES_W-10,10,string(global.hpp) + " Health:", killtextscale,killtextscale,0);
} 

if (room != rMenu) && (instance_exists(oPlayer)) 
{
	killtextscale = max(killtextscale * 0.95, 1);
	DrawSetText(c_black, fMenu, fa_right, fa_top);
	draw_text_transformed(RES_W-8,62,string(global.killsss) + " Kills:", killtextscale,killtextscale,0);
	draw_set_colour(c_white);
	draw_text_transformed(RES_W-10,60,string(global.killsss) + " Kills:", killtextscale,killtextscale,0);
} 