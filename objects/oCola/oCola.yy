{
    "id": "3ac95971-4ab5-4187-a814-328bc22fb6fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCola",
    "eventList": [
        {
            "id": "aae765af-ece3-4bbd-94c4-f052dd14ea49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3ac95971-4ab5-4187-a814-328bc22fb6fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa67eff9-ab35-479a-9b71-fda782af38b1",
    "visible": true
}