{
    "id": "74e688e6-2fbc-4dd6-acca-4f4cf600dfcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet2",
    "eventList": [
        {
            "id": "81aecec9-0c9e-4018-a4a8-3e86c1fb98f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "216b1bcc-8e08-41d6-896a-6de951fbd75c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "74e688e6-2fbc-4dd6-acca-4f4cf600dfcf"
        },
        {
            "id": "5360f19c-5881-4ae5-ad8a-eb1f534fa863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "74e688e6-2fbc-4dd6-acca-4f4cf600dfcf"
        },
        {
            "id": "cc296690-a985-4e06-9056-4f70d628fab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74e688e6-2fbc-4dd6-acca-4f4cf600dfcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2ad1d8e-9eda-4bf9-89b4-e7d04b393c6f",
    "visible": true
}