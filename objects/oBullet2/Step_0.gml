 x += lengthdir_x(3,direction);
 y += lengthdir_y(3,direction);


if (place_meeting(x,y,pShootable))
{
	with (instance_place(x,y,pShootable))
	{
		hp=hp-7   ;
		flash = 3;
		hitfrom = other.direction;
	}

	instance_destroy();
}

if (position_meeting(x,y,oWall)) && (image_index != 0) 
{
	while (position_meeting(x,y,oWall))
	{
		x -= lengthdir_x(10,direction);
		y -= lengthdir_y(10,direction);
	}
	spd = 0;
	instance_change(oHitSpark,true);
	layer_add_instance("Tiles",id);
	depth += 1;
}

