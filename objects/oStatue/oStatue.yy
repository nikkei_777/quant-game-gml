{
    "id": "6e21a970-7032-4698-a8de-685dc95b2c02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStatue",
    "eventList": [
        {
            "id": "507aaf91-e6c7-4e17-ac76-98c7f577f6a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e21a970-7032-4698-a8de-685dc95b2c02"
        },
        {
            "id": "0a36964e-543c-466f-bae9-abe543891237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6e21a970-7032-4698-a8de-685dc95b2c02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a667123c-eef1-4b23-be50-7593b5f86739",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "417bf202-c797-41d2-87eb-47b2a3de2447",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f238566a-e69b-4779-9511-42de0b60f2fb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "e16bef7e-f64d-4966-a066-654cb21466a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "89361970-4bc8-47ec-80d8-b433faf7f68d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fda2ba45-573b-4fa2-bbef-8c50db5ff9d6",
    "visible": true
}