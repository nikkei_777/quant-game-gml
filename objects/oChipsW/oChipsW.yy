{
    "id": "dd457b56-923b-4f24-9dd3-2bb6050e6cd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChipsW",
    "eventList": [
        {
            "id": "47369ab9-110c-4031-a088-bbf83e195c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "dd457b56-923b-4f24-9dd3-2bb6050e6cd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48db4aab-e188-44ae-a18c-7e454a618c6e",
    "visible": true
}