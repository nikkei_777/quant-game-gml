{
    "id": "216b1bcc-8e08-41d6-896a-6de951fbd75c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "torg",
    "eventList": [
        {
            "id": "1a8d93ad-2efe-4389-969d-f9349cb16892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "216b1bcc-8e08-41d6-896a-6de951fbd75c"
        },
        {
            "id": "0e44d9cf-a94d-4a03-9e7b-7293036c6b9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "216b1bcc-8e08-41d6-896a-6de951fbd75c"
        },
        {
            "id": "fba947a5-0639-4f95-844a-0a83f486663e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "216b1bcc-8e08-41d6-896a-6de951fbd75c"
        },
        {
            "id": "152a2a56-e3a2-403a-8af5-771a3ee9b910",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "216b1bcc-8e08-41d6-896a-6de951fbd75c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c629a35b-e88c-4ebf-97af-9cf4d31ec07c",
    "visible": true
}