{
    "id": "1bf75fc2-74ca-4849-9bc0-ca6064f5d8b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGunPickup1",
    "eventList": [
        {
            "id": "be814aa1-72f7-43ab-9cae-edb167bcdbed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bf75fc2-74ca-4849-9bc0-ca6064f5d8b2"
        },
        {
            "id": "99d77da1-de7e-42f9-bb40-5bad955ec356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1bf75fc2-74ca-4849-9bc0-ca6064f5d8b2"
        },
        {
            "id": "14ef1892-755e-422a-93b9-61e1fe921fbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1bf75fc2-74ca-4849-9bc0-ca6064f5d8b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c94d3bff-e16a-4a42-a052-fa6878ffc9b4",
    "visible": true
}