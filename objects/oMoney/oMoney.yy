{
    "id": "88f20f1b-a0a0-4311-84e5-d94c9dd771a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMoney",
    "eventList": [
        {
            "id": "7a45afcd-a9c2-474c-93fa-fafca7d956c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "88f20f1b-a0a0-4311-84e5-d94c9dd771a8"
        },
        {
            "id": "5e074680-1994-4bc6-8691-ed34a03aa336",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "88f20f1b-a0a0-4311-84e5-d94c9dd771a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fde3a8a7-fd2a-4052-96a3-434daa343b8e",
    "visible": true
}