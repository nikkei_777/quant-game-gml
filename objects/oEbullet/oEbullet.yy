{
    "id": "f4263063-2f07-4f9e-bd9f-bb824229c936",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEbullet",
    "eventList": [
        {
            "id": "9631f17f-39c2-4117-b324-674c4a305b86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f4263063-2f07-4f9e-bd9f-bb824229c936"
        },
        {
            "id": "ff3aa9c1-4a80-4874-b243-49857ed5297e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4263063-2f07-4f9e-bd9f-bb824229c936"
        },
        {
            "id": "6d932fc4-3105-4777-a3d1-df801f6faac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f4263063-2f07-4f9e-bd9f-bb824229c936"
        },
        {
            "id": "ebd73db8-112d-4bb1-8588-ee019c04519e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "216b1bcc-8e08-41d6-896a-6de951fbd75c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f4263063-2f07-4f9e-bd9f-bb824229c936"
        }
    ],
    "maskSpriteId": "13c943cd-5f40-4cba-a9ea-b6a21bd5ad37",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "792a9253-a427-4a50-ad93-4710dfe36b36",
    "visible": true
}