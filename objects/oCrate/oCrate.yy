{
    "id": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCrate",
    "eventList": [
        {
            "id": "5a4a45cf-34e7-4372-bf70-6869fbcd66bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3"
        },
        {
            "id": "f643f703-4670-41d5-bae9-40469c0eb2bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3"
        },
        {
            "id": "f5e16772-57dd-4d9b-8e44-bad4ed47846b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f4263063-2f07-4f9e-bd9f-bb824229c936",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3"
        },
        {
            "id": "7dd02f40-f91d-43e7-bef9-7ecd105829ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e6a5411a-37c7-4e5e-bc62-c96fe7f04b4d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3"
        },
        {
            "id": "5602f68d-560e-4155-bd6b-9d49e74a70c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9179a345-ebbe-4d6b-8e0e-46658f10d5b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a667123c-eef1-4b23-be50-7593b5f86739",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d91bc1a-e5d7-404d-a7fa-56d68836b36d",
    "visible": true
}