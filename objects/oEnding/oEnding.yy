{
    "id": "93e6d7bb-3ff1-4719-8c13-2c341c1b5ac6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnding",
    "eventList": [
        {
            "id": "b64b4db6-97f0-4407-a0e4-e539dd15ecda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93e6d7bb-3ff1-4719-8c13-2c341c1b5ac6"
        },
        {
            "id": "db1b4b61-8c16-4fbe-b3f6-59ba3cd166fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93e6d7bb-3ff1-4719-8c13-2c341c1b5ac6"
        },
        {
            "id": "811498f7-cceb-4b2b-b2cd-f9686ccd0467",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "93e6d7bb-3ff1-4719-8c13-2c341c1b5ac6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}