{
    "id": "52e99b5d-0250-4c30-8584-376b59d4bced",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGun",
    "eventList": [
        {
            "id": "6888c779-dd31-423d-b4d8-893d1e5944a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "52e99b5d-0250-4c30-8584-376b59d4bced"
        },
        {
            "id": "417e8489-d1c9-499b-b4ea-3e9895efd95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52e99b5d-0250-4c30-8584-376b59d4bced"
        },
        {
            "id": "3f9d7393-81b0-4be6-8995-1cd351c9f364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52e99b5d-0250-4c30-8584-376b59d4bced"
        },
        {
            "id": "d68939ce-1bf1-4d48-b305-358cdd3eabf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1bf75fc2-74ca-4849-9bc0-ca6064f5d8b2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "52e99b5d-0250-4c30-8584-376b59d4bced"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "61f9ed89-4e4f-4f56-9ed0-8e0e122c95ba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "10420503-eba4-400a-9cb3-f782272c49d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "3a21d7ac-7352-4118-a7f0-549b77a6e356",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 16
        },
        {
            "id": "db3fcf8b-e3a3-4459-999c-4bb7e1e1b204",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c7a880d-aec3-469f-b677-49f4c256833a",
    "visible": true
}