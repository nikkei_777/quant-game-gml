{
    "id": "8e584d1d-3241-4674-adc0-bdaaa0464535",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPDead",
    "eventList": [
        {
            "id": "5b11ac92-8840-4898-983d-796d8425675c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e584d1d-3241-4674-adc0-bdaaa0464535"
        },
        {
            "id": "3d57f573-a83e-42fd-8998-e1d575961c48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e584d1d-3241-4674-adc0-bdaaa0464535"
        },
        {
            "id": "bf17ba61-dfb0-4141-9868-fff2b1a09c17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8e584d1d-3241-4674-adc0-bdaaa0464535"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "462c526c-b261-4c1a-8355-c3494ec14359",
    "visible": true
}