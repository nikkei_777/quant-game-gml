{
    "id": "5f176fee-9e91-4629-ac42-a05d5212f618",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGun1",
    "eventList": [
        {
            "id": "2978122c-6c74-46f8-8a0f-db33ee096710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f176fee-9e91-4629-ac42-a05d5212f618"
        },
        {
            "id": "8b1152d6-1bcd-4e02-b0e8-979b3b3a71c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f176fee-9e91-4629-ac42-a05d5212f618"
        },
        {
            "id": "285f7ca1-3e4a-48ff-8aac-21afa89a273b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "5f176fee-9e91-4629-ac42-a05d5212f618"
        },
        {
            "id": "da8189c0-8af1-471a-b72b-fdc24f8b17da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "60d5f88c-e9ab-4241-96ef-0ebca61957dc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5f176fee-9e91-4629-ac42-a05d5212f618"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20ad0294-be25-4c79-a14c-0987dd6e814b",
    "visible": true
}