{
    "id": "8bc9ef10-48a5-4eb3-9317-3b23e81b5e26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPrimerochnaia",
    "eventList": [
        {
            "id": "43531cab-c55f-457d-bbd0-b6ba4017bc9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bc9ef10-48a5-4eb3-9317-3b23e81b5e26"
        },
        {
            "id": "524525bd-7653-4188-8c8e-4e7d7b5c1e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8bc9ef10-48a5-4eb3-9317-3b23e81b5e26"
        },
        {
            "id": "47a64e34-3d38-44a4-b414-1fd3594288a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bc9ef10-48a5-4eb3-9317-3b23e81b5e26"
        },
        {
            "id": "40944483-b0bc-4aa7-9a3e-5fe32f62db4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8bc9ef10-48a5-4eb3-9317-3b23e81b5e26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "89de584b-c3f3-4bfc-88c5-91513f2b698d",
    "visible": true
}