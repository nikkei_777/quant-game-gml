{
    "id": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "941b22d0-a29d-41d5-a123-afedc9498f7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "89fa040a-00f0-4f20-924c-3ffa8ccee8c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "a7e901de-9da1-4874-9035-6ce22d06f20d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "a33ce254-58f6-4b9e-b6d1-080a39182441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "32d3bb20-0ad3-475e-b761-4375ab88387c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a43ed3ef-76f5-40e7-8b34-e5b374e5b0cb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "80fd867c-c84f-4c83-b671-fd9859c7017b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 9,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "58608d7b-b3a2-41bf-b4eb-590af5fb2be7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f4263063-2f07-4f9e-bd9f-bb824229c936",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        },
        {
            "id": "cf33a42e-7366-4ec5-af79-74e6c5394188",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0ccff1a4-7829-4d8c-848d-a57c5505d186"
        }
    ],
    "maskSpriteId": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3200294b-1b59-4bc1-8c2c-c6dc30e2b5cc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "7b0dc023-f14d-42ae-9eea-e8f0069944aa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "74952d61-0458-442e-bad1-f096636b943b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "1970868d-5ea0-4d87-a6bc-b525cdc6c56f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc0e541e-8462-4dc8-899d-41d0ea6971f7",
    "visible": true
}