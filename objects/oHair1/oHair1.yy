{
    "id": "0c85d8c6-0f50-4755-bed2-3b99c2a54af4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHair1",
    "eventList": [
        {
            "id": "33c8b20b-518e-4aad-9539-5cc548d0e084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0c85d8c6-0f50-4755-bed2-3b99c2a54af4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef7e3368-7fe1-4405-9334-8f6ea0cd75ce",
    "visible": true
}