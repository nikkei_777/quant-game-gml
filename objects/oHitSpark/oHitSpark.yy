{
    "id": "6af7dff2-8863-4210-9765-1608ee47a3c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHitSpark",
    "eventList": [
        {
            "id": "22d1b276-9284-4f9e-b689-2a57534b86ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6af7dff2-8863-4210-9765-1608ee47a3c1"
        },
        {
            "id": "4ac357b6-641f-4425-b605-ffb2149cdadf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6af7dff2-8863-4210-9765-1608ee47a3c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9422c08d-bba2-4128-b482-43ee5686a379",
    "visible": true
}