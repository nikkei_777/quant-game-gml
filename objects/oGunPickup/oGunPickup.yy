{
    "id": "60d5f88c-e9ab-4241-96ef-0ebca61957dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGunPickup",
    "eventList": [
        {
            "id": "2c0d6588-a8fa-4e7a-8c64-1186b818b0af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60d5f88c-e9ab-4241-96ef-0ebca61957dc"
        },
        {
            "id": "ea22bb8f-0ef3-442d-a97b-2851ee7f30dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0ccff1a4-7829-4d8c-848d-a57c5505d186",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60d5f88c-e9ab-4241-96ef-0ebca61957dc"
        },
        {
            "id": "32b49d9b-2a34-42fb-877d-89d644ceb800",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60d5f88c-e9ab-4241-96ef-0ebca61957dc"
        },
        {
            "id": "e4507691-16c5-4b40-8972-82fddf3a44ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "60d5f88c-e9ab-4241-96ef-0ebca61957dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb8a89a3-d649-4bc4-b3a0-11c99cd8295d",
    "visible": true
}