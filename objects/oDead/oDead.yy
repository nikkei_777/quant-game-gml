{
    "id": "fb9ea6e9-f693-43fd-a57f-b1b2e1d141d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDead",
    "eventList": [
        {
            "id": "25157a51-b270-4ce3-8dea-66598b7911a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb9ea6e9-f693-43fd-a57f-b1b2e1d141d1"
        },
        {
            "id": "b70e486d-65e9-49eb-a47a-86d16a40a78d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb9ea6e9-f693-43fd-a57f-b1b2e1d141d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e8feff6-eea1-479a-a2ae-d9adcb781008",
    "visible": true
}